### PartnerWeb Solution
#### About
    -SaaS (S)oftware (A)s (A) (S)ervice)
    The PartnerWeb solution is a SaaS application. 
    The main project in the solution where it contains 
    Tenants for each Partner.

### Modulebased
    PartnerWeb application is modulebased, where modules can be anything.
    In this application there's for eg. the WebApi is included as a Module.
    To add a new module, see specifications in the Modules Readme.

#### Configuration
    In appsettings.json there's a configuration for Tenants and their
    behaviour and rights to access Modules in the Feature section. 
    See below:

### Features
    Below is a line of modules enabled for a specific Partner(Tenant)
    "Features": [ "Onboarding", "Api" ]
    This line gives the Partner(Tenant) rights to use these features.

### Swagger
    Swagger is reached from the base application and displays all 
    Endpoints available in the application, including modules. 
    Swagger API()Endpoit is /swagger


### Modules
##### About
	Modules are extension to the main project. 
	Each module can be added to any Tenant, or remowed.

##### Location
	Modules are placed in the main project, 

### Adding a new Module

	To add more modules, add a new WebApplication project, 
	add the reference to the OrchardCore.Module.Targets,
    and set the <OutputType>Library</OutputType> in the project file.
    You can remove the Program.cs, since this is a module.
    In Startup add :

    public void Configure(IEndpointRouteBuilder endpoints)
    {
        endpoints.MapDefaultControllerRoute();
    }
    Remove all other coding.
    
    Add the module to the main project.
    The module can then be reached from the modulename 
    or the controllers name like /partner/test to see 
    the output from the Webapi's test which exists in 
    the WebApi project.


	