﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PartnerWeb.Data;
using PartnerWeb.Models;
using PartnerWeb.Utils;
using System;
using System.Linq;

namespace Onboarding.Controllers
{
    /// <summary>
    /// Onboarding module.
    /// </summary>
    [Route("onboarding")]
    public class HomeController : Controller
    {
        private readonly IDummyRepository _repository;
        private readonly IMemoryService _cache;
        private Pension Model;
        private readonly HttpContext context;
        private ISession Session;


        public HomeController(
            IDummyRepository repository,
            IMemoryService cache, 
            IHttpContextAccessor httpContext)
        {
            this._repository = repository;
            this._cache = cache;
            this.context = httpContext.HttpContext;
            Session = context.Session;
        }


        [HttpGet]
        [Route("prospect")]
        public IActionResult StartById(string prospectid)
        {
            /*
             * As soon as we hit this route, inject the Session storage.
             */
            context.Session.SetString("ProspectId", prospectid);
            
            var data = _repository;
            var prospect = data.GetProspect(new Guid(prospectid));
            var providers = data.GetProviders;
            var provider = providers.Single(p => p.Id == new Guid(prospect.Employee.Ceeding));

            //Check if user visited before.
            Model =  _cache.Get<Pension>(new Guid(prospectid));
            
            //If not, create new record
            if (Model == null)
            {
                Model = new Pension
                {
                    Company = prospect.Employee.Name,
                    Count = prospect.Employee.Count,
                    Email = prospect.Employee.Email,
                    OrgNo = prospect.Employee.OrgNo,
                    YearlySalary = prospect.Employee.YearlySalary,
                    Ceeding = null ?? Convert.ToString(provider.Id),
                    ProspectId = prospect.Id,
                    SavingsProviders = providers,
                    Agreement = false
                };
                Model = _cache.Create<Pension>(Model, prospect.Id);
            }
                 
            /*
             * Hides the Guid string in url by 
             * redirect to the welcome route.
             */
            return Redirect("welcome");
        }


        /// <summary>
        /// Welcome route
        /// </summary>
        /// <returns></returns>
        [Route("welcome")]
        public IActionResult Index()
        {
            Model = _cache.Get<Pension>(Session.GetString("ProspectId"));
            return View(Model);
        }


        /// <summary>
        /// Order route
        /// </summary>
        /// <returns></returns>
        [Route("bestill-pensjon")]
        public IActionResult StepOne()
        {
            Model = _cache.Get<Pension>(Session.GetString("ProspectId"));
            return View(Model);
        }


        /// <summary>
        /// Save step one
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("steponesave")]
        public IActionResult StepOneSave(Pension model)
        {
            Model = _cache.Get<Pension>(Session.GetString("ProspectId"));
            Model.Company = model.Company;
            Model.OrgNo = model.OrgNo;
            Model.Email = model.Email;
            Model = _cache.Update<Pension>(Model, model.ProspectId);

            return Redirect("ditt-tilbud");
        }


        /// <summary>
        /// Storage to keep data in session.
        /// </summary>
        /// <returns></returns>
        [Route("ditt-tilbud")]
        public IActionResult StepTwo()
        {
            Model = _cache.Get<Pension>(Session.GetString("ProspectId"));
            return View(Model);
        }


        /// <summary>
        /// Save step two
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("StepTwoSave")]
        public IActionResult StepTwoSave(Pension model)
        {
            Model = _cache.Get<Pension>(Session.GetString("ProspectId"));
            Model.YearlySalary = model.YearlySalary;
            Model.Count = model.Count;
            Model = _cache.Update<Pension>(Model, model.ProspectId);

            return Redirect("pensjonsavtale");
        }


        /// <summary>
        /// Route pension deal
        /// </summary>
        /// <returns></returns>
        [Route("pensjonsavtale")]
        public IActionResult StepThree()
        {
            Model = _cache.Get<Pension>(Session.GetString("ProspectId"));
            ViewData["SavingsProviders"] = (string)Model.SavingsProviders.ToLiList(Model.Ceeding);

            return View(Model);
        }


        /// <summary>
        /// Save pension deal
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("StepThreeSave")]
        public IActionResult StepThreeSave(Pension model)
        {
            Model = _cache.Get<Pension>(Session.GetString("ProspectId"));
            Model.Agreement = model.Agreement;
            Model.Ceeding = model.Ceeding;

            Model = _cache.Update<Pension>(Model, model.ProspectId);
          
            return Redirect("signering");
        }


        /// <summary>
        /// From here on we will be redirected to Sign the deal.
        /// </summary>
        /// <returns></returns>
        [Route("signering")]
        public IActionResult StepFour()
        {
            Model = _cache.Get<Pension>(Session.GetString("ProspectId"));

            return View(Model);
        }
    }
}
