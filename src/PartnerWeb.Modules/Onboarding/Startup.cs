using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PartnerWeb.Data;

namespace Onboarding
{
    public class Startup
    {
        private IConfiguration Configuration;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddEntityFrameworkSqlServer()
                .AddDbContext<DataContext>(opts =>
                opts.UseSqlServer(Configuration.GetConnectionString("SPE_PuST_Test"))
            );
        }

        public void Configure(IEndpointRouteBuilder endpoints)
        {
            endpoints.MapDefaultControllerRoute();
        }
    }
}
