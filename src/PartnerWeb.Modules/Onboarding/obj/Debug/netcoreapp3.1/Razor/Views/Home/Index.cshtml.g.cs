#pragma checksum "C:\Users\bootproject\Desktop\PartnerAppV2\src\PartnerWeb.Modules\Onboarding\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1e6c95ffdd82ed2bbfe7de4e1a7b9bdbd7633697"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1e6c95ffdd82ed2bbfe7de4e1a7b9bdbd7633697", @"/Views/Home/Index.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<PartnerWeb.Models.Pension>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\bootproject\Desktop\PartnerAppV2\src\PartnerWeb.Modules\Onboarding\Views\Home\Index.cshtml"
   Layout = "_layout"; 

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            WriteLiteral("\r\n");
#nullable restore
#line 5 "C:\Users\bootproject\Desktop\PartnerAppV2\src\PartnerWeb.Modules\Onboarding\Views\Home\Index.cshtml"
 using (Html.BeginForm("StepOne", "Home"))
{

#line default
#line hidden
#nullable disable
            WriteLiteral(@"    <section class=""sharedSection"">
        <section class=""sectioncontent bottom"">
            <!---->
            <center>
                
                <h1>Velkommen!</h1>
               
                <p>I denne veiviseren får du mulighet til å bestille pensjon fra DUVI fullintegrert med Fiken. For å gjøre det enkelt for deg, hentes alle data fra Fiken – du trenger bare å klikke deg igjennom.</p>
                <br/><br />
                <p><input type=""submit"" class=""btn valid"" value=""Neste"" /></p>
            </center>
            <!---->
        </section>
    </section>
");
#nullable restore
#line 21 "C:\Users\bootproject\Desktop\PartnerAppV2\src\PartnerWeb.Modules\Onboarding\Views\Home\Index.cshtml"
Write(Html.HiddenFor(c => c.Company));

#line default
#line hidden
#nullable disable
#nullable restore
#line 22 "C:\Users\bootproject\Desktop\PartnerAppV2\src\PartnerWeb.Modules\Onboarding\Views\Home\Index.cshtml"
Write(Html.HiddenFor(c => c.Count));

#line default
#line hidden
#nullable disable
#nullable restore
#line 23 "C:\Users\bootproject\Desktop\PartnerAppV2\src\PartnerWeb.Modules\Onboarding\Views\Home\Index.cshtml"
Write(Html.HiddenFor(c => c.CurrentProvider));

#line default
#line hidden
#nullable disable
#nullable restore
#line 24 "C:\Users\bootproject\Desktop\PartnerAppV2\src\PartnerWeb.Modules\Onboarding\Views\Home\Index.cshtml"
Write(Html.HiddenFor(c => c.Email));

#line default
#line hidden
#nullable disable
#nullable restore
#line 25 "C:\Users\bootproject\Desktop\PartnerAppV2\src\PartnerWeb.Modules\Onboarding\Views\Home\Index.cshtml"
Write(Html.HiddenFor(c => c.OrgNo));

#line default
#line hidden
#nullable disable
#nullable restore
#line 26 "C:\Users\bootproject\Desktop\PartnerAppV2\src\PartnerWeb.Modules\Onboarding\Views\Home\Index.cshtml"
Write(Html.HiddenFor(c => c.YearlySalary));

#line default
#line hidden
#nullable disable
#nullable restore
#line 27 "C:\Users\bootproject\Desktop\PartnerAppV2\src\PartnerWeb.Modules\Onboarding\Views\Home\Index.cshtml"
Write(Html.HiddenFor(c => c.ProspectId));

#line default
#line hidden
#nullable disable
#nullable restore
#line 27 "C:\Users\bootproject\Desktop\PartnerAppV2\src\PartnerWeb.Modules\Onboarding\Views\Home\Index.cshtml"
                                      
}

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<PartnerWeb.Models.Pension> Html { get; private set; }
    }
}
#pragma warning restore 1591
