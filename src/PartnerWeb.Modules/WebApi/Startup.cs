using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;


namespace WebApi
{
    public class Startup
    {
        public void Configure(IEndpointRouteBuilder endpoints)
        {
            endpoints.MapDefaultControllerRoute();
        }
    }
}
