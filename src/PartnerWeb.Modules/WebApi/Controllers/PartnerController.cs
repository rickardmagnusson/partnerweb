﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PartnerWeb.Models;
using PartnerWeb.Utils;

namespace WebApi.Controllers
{
    /// <summary>
    /// Contains methods to create Prospects 
    /// and return callbacks for partners.
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class PartnerController : ControllerBase
    {
        /// <summary>
        /// Route /Partner/GetCallBackUrl
        /// </summary>
        /// <returns></returns>
        [HttpPost] //Should be a post
        //[Authorize(Roles = "DuviShopPartners")]
        [Authorize(Policy = "ValidAccessToken")]
        [Route("GetCallBackUrl")]
        public IActionResult Get(PartnerWebRequest requestmodel)
        {
            //Do something with the requestmodel... Then --->

            /*
             * Return model.
             */
            var response = new PartnerWebResponse
            {
                CallbackURL = $"https://duvi.no/partnerweb/product?{Guid.NewGuid()}"
            };

            return Ok(response);
        }


        /// <summary>
        /// Extension function to generate a valid but 
        /// fake Norwegian SSN. 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSSN")]
        public IActionResult GetSSN()
        {
            var model = Extensions.GenSSN();

            return Ok(model);
        }


        /// <summary>
        /// Testfunction. 
        /// Route /Partner/Test
        /// </summary>
        /// <returns>A test CallbackURL</returns>
        [HttpGet]
        [Route("Test")]
        public IActionResult Test()
        {
            /*
             * A simple return model.
             */
            var response = new PartnerWebResponse
            {
                CallbackURL = $"https://duvi.no/partnerweb/product?{Guid.NewGuid()}"
            };

            return Ok(response);
        }
    }
}
