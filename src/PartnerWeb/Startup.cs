using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using PartnerWeb.Data;
using PartnerWeb.Services;
using System;

namespace PartnerWeb
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRouting();

            /*
             * Adds a fake dummy repository for testing.
             */
            services.AddSingleton<IDummyRepository, DummyRepository>();

            /*
             * Add memory caching.
             */
            services.AddMemoryCache();
            services.AddSingleton<IMemoryService, MemoryService>();

            /*
             * Module view location overrides.
             */
            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.ViewLocationExpanders.Add(new TenantViewLocationExpander());
            });

            /*
             * Session management.
             * A session will live for 30 days unless the server is restarted.
             */
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromDays(30);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });

            /*
            * This hides the PwaController from ApiExplorer which is used by Swashbuckle.
            */
            services.AddMvc(c => c.Conventions.Add(new ApiExplorerIgnores()));

            services.AddOrchardCore()
               .Configure((tenantAppBuilder, endpointBuilder, tenantScopeServiceProvier) =>
               {

               }).ConfigureServices(tenantServices =>
               {
                   tenantServices.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

                   tenantServices.AddAuthentication();
                   tenantServices.AddAuthorization();

                   /*
                    * Add Swagger API
                    */
                   tenantServices.AddSwaggerGen();

                   tenantServices.Configure<RazorViewEngineOptions>(o =>
                   {
                       o.ViewLocationExpanders.Add(new TenantViewLocationExpander());
                   });


               }).AddMvc()
                 .WithTenants(); //Remove or add multitenancy
        }


        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseStaticFiles();
            app.UseRouting();
            app.UseSession();
            
            /*
             * Use multitenancy
             */
            app.UseOrchardCore(apps =>
            {
                /*
                 * Swagger JSON (OpenAPI) endpoint.
                 */
                apps.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "PartnerWeb API V1");
                });

                apps.UseAuthentication();
                apps.UseAuthorization();

            });
        }
    } 
}
