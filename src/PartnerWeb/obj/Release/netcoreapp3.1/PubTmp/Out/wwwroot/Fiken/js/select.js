/*Dropdown Menu*/
$('.dropdown').click(function () {
    if ($(this).hasClass('disabled'))
        return false;
    $(this).attr('tabindex', 1).focus();
    $(this).toggleClass('active');
    $(this).find('.dropdown-menu').slideToggle(10);
});

$('.dropdown').focusout(function () {
    $(this).removeClass('active');
    $(this).find('.dropdown-menu').slideUp(10);
});

$('.dropdown .dropdown-menu li').click(function () {
    $(this).parents('.dropdown').find('span').text($(this).text());
    $(this).parents('.dropdown').find('input').attr('value', $(this).attr('id'));
});

$(document).ready(function () {
    var val = $('.dropdown .dropdown-menu li.selected');
    $(val).parents('.dropdown').find('span').text(val.text());
});



