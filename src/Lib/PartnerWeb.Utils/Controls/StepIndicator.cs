﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace PartnerWeb.Utils
{
    /// <summary>
    /// A simple number indication of steps.
    /// Dependent on CSS.
    /// <para>Author: Rickard Magnusson</para>
    /// <para>Date created: 2020-09-27</para/>
    /// <para>Email: bootproject@icloud.com</para> 
    /// </summary>
    [HtmlTargetElement("stepindicator")]
    public class StepIndicator : TagHelper
    {
        #region Fields


        /// <summary>
        /// Optional Id to be added to control.
        /// </summary>
        public string Id { get; set; } = nameof(StepIndicator).ToCamelCase();

        /// <summary>
        /// The current step.
        /// Default (0)
        /// </summary>
        [HtmlAttributeName("current")]
        public int Current { get; set; } = 0;


        /// <summary>
        /// Amount(Count) of steps to create.
        /// Default (1)
        /// </summary>
        [HtmlAttributeName("step-count")]
        public int StepCount { get; set; } = 1;


        /// <summary>
        /// Let you override the default css class naming.
        /// </summary>
        public string NavCssClassDefault { get; set; } = "stepindicator";


        /// <summary>
        /// Let you override the default css class naming.
        /// </summary>
        public string CssClassDefault { get; set; } = "stepindicator-horizontal";


        /// <summary>
        /// Let you override the default css class naming.
        /// </summary>
        public string ProgressCssClassDefault { get; set; } = "progressline";


        /// <summary>
        /// Let you override the default css class naming.
        /// </summary>
        public string DefaultCssActiveClass { get; set; } = "active";


        /// <summary>
        ///The text: for eg. Step {0} of {1}.
        /// </summary>
        public string FormText { get; set; } = "Step {0} of {1}";

        #endregion

        #region Process

        /// <summary>
        /// Override the default output.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="output"></param>
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "nav";
            output.Attributes.Add("data-role", NavCssClassDefault);
            output.Attributes.Add("id", Id);
            output.Attributes.Add("class", NavCssClassDefault);
            output.TagMode = TagMode.StartTagAndEndTag;
            output.PreContent.SetHtmlContent(CreateSteps().InnerHtml);
        }


        #endregion

        #region Private methods

        /// <summary>
        /// Creates the inner html tags.
        /// </summary>
        /// <returns></returns>
        private TagBuilder CreateSteps()
        {
            /*
             * Default container to add html tags.
             */
            var container = new TagBuilder("div");
            var text = string.Format("<div class='steps'>{0}</div>", string.Format(FormText, Current, StepCount));
            container.InnerHtml.AppendHtml(text);
            var ul = new TagBuilder("ul");
            ul.AddCssClass(CssClassDefault);

            /*
             * Create the steps.
             */
            for (var i = 1; i <= StepCount; i++)
            {
                var li = new TagBuilder("li");

                /*
                 * Add the class "active" to element if 
                 * it's inside the array of current step.
                 */
                if (i <= Current)
                    li.AddCssClass(string.Format("step {0}", DefaultCssActiveClass));
                else
                    li.AddCssClass("step");

                var span = new TagBuilder("span");
                span.InnerHtml.Append(string.Format("{0}", i)); //failsafe of null on number.
                li.InnerHtml.AppendHtml(span);
                ul.InnerHtml.AppendHtml(li);
            }

            /*
             * Progress line
             */
            var progress = new TagBuilder("div");
            progress.AddCssClass(ProgressCssClassDefault);

            /*
             * Add elements to container.
             */
            container.InnerHtml.AppendHtml(progress);
            container.InnerHtml.AppendHtml(ul);

            return container;
        }


        #endregion
    }
}
