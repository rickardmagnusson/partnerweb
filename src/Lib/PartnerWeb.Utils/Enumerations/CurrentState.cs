﻿
namespace PartnerWeb.Utils
{
    public enum CurrentState
    {
        NONE = 0,
        ORGANIZATION = 1,
        PERSONAL = 2,
        OFFER = 3,
        SIGN = 4
    }
}
