﻿
/// <summary>
/// Namespace that can be reached in whole project.
/// </summary>
namespace PartnerWeb
{
    /// <summary>
    /// Environment cases
    /// </summary>
    public enum Environment
    {
        /// <summary>
        /// TEST Environment
        /// </summary>
        TEST,

        /// <summary>
        /// STAGING Environment
        /// </summary>
        STAGING,

        /// <summary>
        /// Production Environment
        /// </summary>
        PRODUCTION,
    }
}
