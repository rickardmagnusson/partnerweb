﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace PartnerWeb.Utils
{
    /// <summary>
    /// Contains various string extensions.
    /// </summary>
    public static partial class StringExtensions
    {
        public static string ToCamelCase(this string name)
        {
            var sb = new StringBuilder();
            var i = 0;
            /* 
             * While we encounter upper case characters
             * (except for the last), convert to lowercase.
             */
            while (i < name.Length - 1 && char.IsUpper(name[i + 1]))
            {
                sb.Append(char.ToLowerInvariant(name[i]));
                i++;
            }

            /* 
             *  Copy the rest of the characters as is, 
             *  except if we're still on the first character 
             *  - which is always lowercase.
             */
            while (i < name.Length)
            {
                sb.Append(i == 0 ? char.ToLowerInvariant(name[i]) : name[i]);
                i++;
            }

            return sb.ToString();
        }


        public static Guid ToGuid(this byte[] data)
        {
            return new Guid(data);
        }
    }
}
