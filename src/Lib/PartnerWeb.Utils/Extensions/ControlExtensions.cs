﻿using Microsoft.AspNetCore.Mvc.Rendering;
using PartnerWeb.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PartnerWeb.Utils
{
    public static class ControlExtensions
    {
        public static List<SelectListItem> ToSelectList(this Dictionary<string, string> items, string selected)
        {
            var list = new List<SelectListItem>();

            items.ToList().ForEach(item => { list.Add(
                new SelectListItem
                {
                    Value = item.Key,
                    Text = item.Value,
                    Selected = (item.Key == selected)
                });
            });
           
            return list;
        }

        public static List<SelectListItem> ToSelectList(this IEnumerable<SavingsProvider> providers, string selected)
        {
            var items = new Dictionary<string, string>();
            providers.ToList().ForEach(provider => { items.Add(provider.Id.ToString(), provider.Name); });

            return items.ToSelectList(Convert.ToString(selected));
        }


        public static string ToLiList(this IEnumerable<SavingsProvider> providers, string selected)
        {
            var menu = new StringBuilder();
            menu.AppendLine("<li>-- Velg avgivende selskap --</li>");

            providers
                .OrderBy(o => o.Name)
                .ToList()
                .ForEach(provider => {

                    menu.AppendFormat("<li id='{0}'{2}>{1}</li>\n", 
                        provider.Id, 
                        provider.Name,
                       (provider.Id == new Guid(selected)) ? " class='selected'" : "");
            });
            
            return menu.ToString();
        }
    }
}
