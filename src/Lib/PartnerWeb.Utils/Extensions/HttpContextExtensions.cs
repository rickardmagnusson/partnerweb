﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using System;

namespace PartnerWeb.Utils
{
    public static class HttpContextExtensions
    {
        public static T Model<T>(this MemoryCache cache, HttpContext context)
        {
            return cache.Get<T>(context.Session.Get<Guid>("ProspectId"));
        }
    }
}
