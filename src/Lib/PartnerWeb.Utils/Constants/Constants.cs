﻿
/// <summary>
/// Namespace that can be reached in whole project.
/// </summary>
namespace PartnerWeb
{
    /// <summary>
    /// Class contaning contstants.
    /// </summary>
    public static partial class Constants
    {
        public const string HttpContextTenantKey = "THEMEVIEWLOCATION";
    }
}
