﻿using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.DependencyInjection;
using OrchardCore.Environment.Shell;
using System.Collections.Generic;

namespace PartnerWeb.Services
{
    /// <summary>
    /// Overrides the default View locations. 
    /// </summary>
    public class TenantViewLocationExpander : IViewLocationExpander
    {
        private const string _moduleKey = "module";

        public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {
            if (context.Values.ContainsKey(_moduleKey))
            { var module = context.Values[_moduleKey];
               
                /*
                 * Override default locations.
                 * If you want to serve the layout from main application,
                 * you can remove the shared folder from here.
                 */
                if (!string.IsNullOrWhiteSpace(module))
                {
                    var moduleViewLocations = new string[]
                    {
                       "/Views/"+module+"/{1}/{0}.cshtml",
                       "/Views/"+module+"/Shared/{0}.cshtml"
                    };
                    return moduleViewLocations;
                }
            }
            return viewLocations;
        }

        public void PopulateValues(ViewLocationExpanderContext context)
        {
            var shellSettings = context.ActionContext.HttpContext.RequestServices.GetRequiredService<ShellSettings>();
            var tenant = shellSettings.Name;

            if (tenant != "Default")
                context.Values[_moduleKey] = tenant;
        }
    }
}
