﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;


namespace PartnerWeb.Data
{
    public interface IMemoryService
    {
        T Create<T>(T model, Guid id);

        T Update<T>(T model, Guid id);

        T Get<T>(Guid id);

        T Get<T>(string id);
    }

    public class MemoryService : IMemoryService
    {
        private IMemoryCache _cache;
        private MemoryCacheEntryOptions _cacheExpirationOptions;
   
        public MemoryService()
        {
            _cacheExpirationOptions = new MemoryCacheEntryOptions();
            _cacheExpirationOptions.AbsoluteExpiration = DateTime.Now.AddMinutes(36000);
            _cacheExpirationOptions.Priority = CacheItemPriority.Normal;
            _cache = new MemoryCache(new MemoryCacheOptions());
        }

        public T Create<T>(T model, Guid id)
        {
            if (_cache.Get<T>(id) == null) {
                return _cache.GetOrCreate<T>(id,
                    cacheEntry => {
                        return model;
                    });
            } else {
                return Update<T>(model, id);
            }
        }

        public T Update<T>(T model, Guid id)
        {
            return _cache.Set(id, model, _cacheExpirationOptions);
        }

        public T Get<T>(Guid id)
        {
            return (T)_cache.Get<T>(id);
        }


        public T Get<T>(string id)
        {
            return (T)_cache.Get<T>(new Guid(id));
        }
    }
}
