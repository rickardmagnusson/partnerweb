﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace PartnerWeb.Data
{

    public interface IDummyRepository
    {
        IEnumerable<Prospect> GetProspects();
        IEnumerable<SavingsProvider> GetProviders { get; }
        IEnumerable<Employee> GetEmployees();
        Employee GetEmployee(Guid guid);
        Prospect GetProspect(Guid id);
    }

    public class DummyRepository : IDummyRepository
    { 
        public IEnumerable<Prospect> GetProspects(){ return Prospect.GetProspects();}

        public IEnumerable<SavingsProvider> GetProviders { get { return SavingsProvider.Providers(); } }

        public IEnumerable<Employee> GetEmployees() { return Employee.GetEmployees(); }

        public Employee GetEmployee(Guid guid) { return Employee.GetEmployee(guid); }

        public Prospect GetProspect(Guid id) { return Prospect.GetProspect(id); }
    }



    //Relation between Prospect and Employee
    public class Prospect
    {
        public Guid Id { get; set; }

        public SavingsProvider Provider { get; set; }

        public Employee Employee { get; set; }

        public Guid GetGuid(string guid)
        {
            return this.Id;
        }



        public static IEnumerable<Prospect> GetProspects() 
        {
            var list = new List<Prospect>
            {
                new Prospect{ Id = new Guid("26a18c88-61c0-410a-ac16-420880dfcc1d"), 
                              Employee = Employee.GetEmployee(new Guid("34853d17-6db5-44ff-b2a8-1058e0e4208e"))
                },
                new Prospect{ Id = new Guid("950e7fec-a458-44cd-91ce-c0703c832908"),
                              Employee = Employee.GetEmployee(new Guid("f4295dea-69e2-4ab2-ba26-d64c611b9d1b"))
                },
                new Prospect{ Id = new Guid("06024318-e604-4166-b3a5-6d24e7306762"),
                              Employee = Employee.GetEmployee(new Guid("647b753e-65ca-4ccf-9055-93bf492d6a3a"))
                }
            };

            return list;
        }

        public static Prospect GetProspect(Guid id)
        {
            return GetProspects().Single(p => p.Id == id);
        }

    }

    public class Employee
    {
        public Guid Id { get; set; }

        [DisplayName("Firmanavn")]
        public string Name { get; set; }

        [DisplayName("Org.nr.")]
        public string OrgNo { get; set; }

        [DisplayName("E-post")]
        public string Email { get; set; }

        [DisplayName("Antall ansatte")]
        public int Count { get; set; }

        [DisplayName("Gj. snitt årslønn")]
        public double YearlySalary { get; set; }

        public bool ReplacePension { get; set; }

        public string Ceeding { get; set; }


        public static IEnumerable<Employee> GetEmployees()
        {
            var list = new List<Employee>
            {
                new Employee{ Ceeding = "26a18c88-61c0-410a-ac16-420880dfcc1d", Id = new Guid("34853d17-6db5-44ff-b2a8-1058e0e4208e"), Count= 10, Email = "first@duvi.no", Name = "My company one", OrgNo = "111111111", ReplacePension= true, YearlySalary = 450000.0 },
                new Employee{ Ceeding = "950e7fec-a458-44cd-91ce-c0703c832908", Id = new Guid("f4295dea-69e2-4ab2-ba26-d64c611b9d1b"), Count= 10, Email = "second@duvi.no", Name = "My company two", OrgNo = "222222222", ReplacePension= true, YearlySalary = 750000.0 },
                new Employee{ Ceeding = "06024318-e604-4166-b3a5-6d24e7306762", Id = new Guid("647b753e-65ca-4ccf-9055-93bf492d6a3a"), Count= 10, Email = "third@duvi.no", Name = "My company three", OrgNo = "333333333", ReplacePension= true, YearlySalary = 630000.0 },
            };

            return list;
        }

        public static Employee GetEmployee(Guid guid)
        {
            return GetEmployees().Single(e => e.Id == guid);
        }

    }



    public class SavingsProvider
    {
       public Guid Id { get; set; }
       public string Name { get; set; }
        public string Selected { get; set; }

        public static IEnumerable<SavingsProvider> Providers()
        {
            var list = new List<SavingsProvider>
            {
                new SavingsProvider{ Id = new Guid("26a18c88-61c0-410a-ac16-420880dfcc1d"), Name = "Duvi Pensjon" },
                new SavingsProvider{ Id = new Guid("06024318-e604-4166-b3a5-6d24e7306762"), Name = "DNB Pensjon" },
                new SavingsProvider{ Id = new Guid("950e7fec-a458-44cd-91ce-c0703c832908"), Name = "Nordea Forsikring" },
                new SavingsProvider{ Id = new Guid("bd48e720-5be0-4a21-96b9-b442cc9c97d7"), Name = "ASAP Forsiking" },
                new SavingsProvider{ Id = new Guid("07a7b821-828b-40ea-b8c7-cdd79d075752"), Name = "Home Forsiking" },
                new SavingsProvider{ Id = new Guid("5d53b33b-b9aa-42f6-91a3-74c106076b78"), Name = "No Forsiking" }
            };

            return list;
        }
    }
}
