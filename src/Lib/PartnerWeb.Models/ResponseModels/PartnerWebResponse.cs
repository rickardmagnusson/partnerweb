﻿
namespace PartnerWeb.Models
{
    /// <summary>
    /// PartnerWeb response model for partners.
    /// Creates a new Prospect
    /// </summary>
    public class PartnerWebResponse
    {
        public string CallbackURL { get; set; }
        public string Message { get; set; } = "Ok";
    }
}
