﻿
namespace PartnerWeb.Models
{
    public class PartnerWebRequest
    {
        public string Agent { get; set; }
        public string Name { get; set; }
        public string OrgNo { get; set; }
        public string CallbackURL { get; set; }
        public int YearlySalary { get; set; }
        public string Ceding { get; set; }
    }
}
