﻿using PartnerWeb.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PartnerWeb.Models
{
    public class Pension
    {
        [Required(ErrorMessage = "*")]
        [DisplayName("Firmanavn")]
        public string Company { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(9, ErrorMessage = "*", MinimumLength = 9)]
        [DisplayName("Org.nr.")]
        public string OrgNo { get; set; }

        [EmailAddress(ErrorMessage = "*")]
        [Required(ErrorMessage = "*")]
        [DisplayName("E-post")]
        public string Email { get; set; }

        [EmailAddress(ErrorMessage = "*")]
        [Required(ErrorMessage = "*")]
        [DisplayName("Gj. snitt årslønn")]
        public double YearlySalary { get; set; } = 550000.0;


        [EmailAddress(ErrorMessage = "*")]
        [Required(ErrorMessage = "*")]
        [DisplayName("Antall ansatte")]
        public int Count { get; set; } = 10;

        [DisplayName("Dagens tilbyder")]
        public string Ceeding { get; set; }

        public bool Agreement { get; set; }


        public Guid ProspectId { get; set; }
        public SavingsProvider CurrentProvider { get; set; }
        public IEnumerable<SavingsProvider> SavingsProviders { get; set; }

    }
}
